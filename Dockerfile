FROM golang:alpine as builder

RUN apk --no-cache add git

#Get sources
RUN go get github.com/mholt/caddy/caddy
RUN go get github.com/caddyserver/builds
RUN go get github.com/filebrowser/caddy

#Workaround for broken filebrowser dependencies
#RUN cd /go/src/github.com/gohugoio/hugo/ && git checkout v0.49.2
#RUN cd /go/src/github.com/mholt/archiver/ && git checkout v2.1.0


#Checkout working plugin versions
#RUN cd /go/src/github.com/filebrowser/caddy && git checkout v1.10.0
#RUN cd /go/src/github.com/filebrowser/filebrowser && git checkout v1.10.0

#Add plugins to build
RUN printf "package caddyhttp\nimport _ \"github.com/filebrowser/caddy\"" > \
        /go/src/github.com/mholt/caddy/caddyhttp/filebrowser.go 

#Build caddy
RUN cd /go/src/github.com/mholt/caddy/caddy && go run build.go

FROM alpine

COPY --from=builder /go/src/github.com/mholt/caddy/caddy/caddy /usr/bin/caddy

# Install other plugin requirements
RUN apk add --no-cache openssh-client git hugo ruby ruby-irb ruby-rdoc

# Install jekyll
RUN apk add --no-cache --virtual .build-deps  build-base ruby-dev && \
 gem install jekyll bigdecimal bundler && \
 apk del .build-deps

COPY Caddyfile /etc/Caddyfile

ENTRYPOINT ["caddy"]
CMD ["--conf","/etc/Caddyfile","--log","stdout"]